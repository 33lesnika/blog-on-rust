use axum::{Json, Router, routing::get, routing::post};
use axum::extract::State;
use log::info;
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use tower_http::cors::{Any, CorsLayer};

use blog::{create_post, DbPool, get_5_posts, get_connection_pool};
use blog::models::{NewPost, Post};

#[tokio::main]
async fn main() {
    let connection_pool = get_connection_pool();

    let cors = CorsLayer::new().allow_origin(Any);
    // build our application with a single route
    let app = Router::new()
        .route("/", get(|| async { "Hello, World!" }))
        .route("/data", get(get_data))
        .route("/user", post(post_data))
        .route("/post", get(get_posts_db))
        .route("/post", post(create_new_post))
        .layer(cors)
        .with_state(connection_pool)
        ;

    // run it with hyper on localhost:3000
    axum::Server::bind(&"0.0.0.0:3000".parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn get_data() -> Json<Value> {
    Json(json!({ "data": 42 }))
}

#[derive(Serialize, Deserialize)]
struct User {
    name: String,
}

async fn post_data(Json(payload): Json<User>) -> Json<Value> {
    let name = payload.name;
    info! {"Name is: {}", name}
    Json(json!({ "data": 42 }))
}

async fn get_posts_db(State(pool): State<DbPool>) -> Json<Vec<Post>> {
    Json(get_5_posts(&pool).await)
}

#[axum_macros::debug_handler]
async fn create_new_post(State(pool): State<DbPool>, Json(payload): Json<NewPost>) -> Json<Post> {
    Json(create_post(&pool, payload).await)
}
