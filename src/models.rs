use chrono::{DateTime, Local};
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use uuid::Uuid;
use crate::schema::posts;

#[derive(Queryable)]
#[derive(Serialize, Deserialize, Debug)]
pub struct Post {
    pub id: Uuid,
    pub author: String,
    pub text: String,
    pub date: DateTime<Local>,
}

#[derive(Insertable, Deserialize)]
#[diesel(table_name = posts)]
pub struct NewPost {
    pub author: String,
    pub text: String,
}
