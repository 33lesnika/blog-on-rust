// @generated automatically by Diesel CLI.

diesel::table! {
    posts (id) {
        id -> Uuid,
        author -> Varchar,
        text -> Text,
        date -> Timestamptz,
    }
}
