use std::env;

use diesel::{QueryDsl};
use diesel_async::{RunQueryDsl, AsyncPgConnection};
use diesel_async::pooled_connection::AsyncDieselConnectionManager;
use diesel_async::pooled_connection::deadpool::Pool;
use dotenv::dotenv;

use crate::models::{NewPost, Post};

pub mod models;
pub mod schema;

type DbConnection = AsyncPgConnection;
pub type DbPool = Pool<DbConnection>;

pub async fn get_5_posts(pool: &DbPool) -> Vec<Post> {
    use self::schema::posts::dsl::*;
    let mut conn = pool.get()
        .await
        .expect("Cannot get db connection");
    let results = posts
        .limit(5)
        .load::<Post>(&mut conn)
        .await
        .expect("Error loading posts");

    println!("Displaying {} posts", results.len());
    results.into_iter()
        .collect::<Vec<Post>>()
        .try_into()
        .unwrap()
}

pub async fn create_post(pool: &DbPool, new_post: NewPost) -> Post {
    use crate::schema::posts;
    let mut conn = pool.get()
        .await
        .expect("Cannot get db connection");
    diesel::insert_into(posts::table)
        .values(&new_post)
        .get_result(&mut conn)
        .await
        .expect("Error saving new post")
}

pub fn get_connection_pool() -> DbPool {
    dotenv().ok();
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let manager = AsyncDieselConnectionManager::<DbConnection>::new(database_url);
    Pool::builder(manager)
        .build()
        .expect("Could not build connection pool")
}
