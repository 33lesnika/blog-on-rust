-- Your SQL goes here
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE posts
(
    id     uuid PRIMARY KEY default uuid_generate_v4(),
    author VARCHAR                  NOT NULL,
    text   TEXT                     NOT NULL,
    date   timestamp with time zone NOT NULL DEFAULT current_timestamp
);